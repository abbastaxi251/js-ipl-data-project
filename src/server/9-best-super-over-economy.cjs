const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathDeliveries = path.join(__dirname, "../data/deliveries.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/9-best-super-over-economy.json"
// )

const csvDeliveries = fs.readFileSync(csvPathDeliveries, "utf-8")

const jsonDeliveries = Papa.parse(csvDeliveries, {
    header: true,
})

const best_super_over_economy_func = () => {
    const bowler_data = jsonDeliveries.data
        .filter((deliveries) => {
            return Number(deliveries.is_super_over) == 1
        })
        .map((delivery) => {
            return [delivery.bowler, delivery.total_runs]
        })
        .reduce((acc, deliveries) => {
            const bowler = deliveries[0]
            const run = Number(deliveries[1])

            if (!acc[bowler]) {
                acc[bowler] = {
                    runs: run,
                    balls: 1,
                    economy: undefined,
                }
            } else {
                if (!acc[bowler].economy) {
                    acc[bowler].economy =
                        (acc[bowler].run / acc[bowler].balls) * 6
                }
                acc[bowler].runs += run
                acc[bowler].balls++
                acc[bowler].economy = (acc[bowler].runs / acc[bowler].balls) * 6
            }

            return acc
        }, {})

    const bowler_dataArray = Object.entries(bowler_data)
        .map(([player, stats]) => {
            return { bowler: player, ...stats }
        })
        .sort((a, b) => a.economy - b.economy)
        .slice(0, 1)

    return bowler_dataArray
}

// const best_super_over_economy = best_super_over_economy_func(jsonDeliveries)

// fs.writeFileSync(jsonPath, JSON.stringify(best_super_over_economy, null, 2))

module.exports = best_super_over_economy_func
