const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathMatches = path.join(__dirname, "../data/matches.csv")
const csvPathDeliveries = path.join(__dirname, "../data/deliveries.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/3-extra-runs-per-team-2016.json"
// )

const csvMatches = fs.readFileSync(csvPathMatches, "utf-8")
const csvDeliveries = fs.readFileSync(csvPathDeliveries, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})

const jsonDeliveries = Papa.parse(csvDeliveries, {
    header: true,
})

// console.log(jsonMatches.data)
// console.log(jsonDeliveries.data)
const extra_runs_per_team_2016_func = () => {
    const matches_id_2016 = jsonMatches.data
        .filter((match) => {
            return match.season == "2016"
        })
        .map((match) => {
            return match.id
        })

    // console.log(matches_id_2016)
    // console.log(jsonDeliveries.data)

    const deliveries_2016 = jsonDeliveries.data
        .filter((deliveries) => {
            return matches_id_2016.includes(String(deliveries.match_id))
        })
        // console.log(deliveries_2016)

        .reduce((accumulator, currentMatch) => {
            const bowling_team = currentMatch.bowling_team
            const extra_runs = Number(currentMatch.extra_runs)

            if (!accumulator[bowling_team]) {
                accumulator[bowling_team] = extra_runs
            } else {
                accumulator[bowling_team] += extra_runs
            }

            return accumulator
        }, {})

    return deliveries_2016
}
// console.log(deliveries_2016)

// const extra_runs_per_team_2016 = extra_runs_per_team_2016_func(jsonMatches,jsonDeliveries)

// fs.writeFileSync(jsonPath, JSON.stringify(extra_runs_per_team_2016, null, 2))

module.exports = extra_runs_per_team_2016_func
