const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPath = path.join(__dirname, "../data/matches.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/2-matches-per-team-per-year.json"
// )

const csvMatches = fs.readFileSync(csvPath, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})
// console.log(jsonMatches.data)

const matches_won_per_team_per_year_func = () => {
    const win_team_year = jsonMatches.data
        .map((match) => [match.winner, match.season])
        .reduce((accumulator, currentMatch) => {
            const team = currentMatch[0]
            const year = currentMatch[1]

            if (!accumulator[team]) {
                accumulator[team] = {} //* assign empty object to further assign keys
            }

            if (!accumulator[team][year]) {
                accumulator[team][year] = 1
            } else {
                accumulator[team][year]++
            }

            return accumulator
        }, {})

    return win_team_year
}
// console.log(matches_per_team_per_year)

// const matches_won_per_team_per_year =
//     matches_per_team_per_year_func(jsonMatches)

// fs.writeFileSync(
//     jsonPath,
//     JSON.stringify(matches_won_per_team_per_year, null, 2)
// )

module.exports = matches_won_per_team_per_year_func