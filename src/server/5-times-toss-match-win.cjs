const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathMatches = path.join(__dirname, "../data/matches.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/5-times-toss-match-win.json"
// )

const csvMatches = fs.readFileSync(csvPathMatches, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})
const times_toss_match_win_func = () => {
    //initially filtering matches data for only when toss winner is the match winner and then using reduce to get the count for the same
    const match_and_toss_win = jsonMatches.data
        .filter((match) => {
            return match.toss_winner == match.winner
        })
        .map((results) => results.toss_winner)
        .reduce((acc, results) => {
            if (!acc[results]) {
                acc[results] = 1
            } else {
                acc[results] += 1
            }
            return acc
        }, {})

    return match_and_toss_win
}
// console.log(match_and_toss_win)

// const times_toss_match_win = times_toss_match_win_func(jsonMatches)

// fs.writeFileSync(jsonPath, JSON.stringify(times_toss_match_win, null, 2))

module.exports = times_toss_match_win_func
