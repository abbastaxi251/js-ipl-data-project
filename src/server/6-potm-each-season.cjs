const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathMatches = path.join(__dirname, "../data/matches.csv")
const csvPathDeliveries = path.join(__dirname, "../data/deliveries.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/6-potm-each-season.json"
// )

const csvMatches = fs.readFileSync(csvPathMatches, "utf-8")
const csvDeliveries = fs.readFileSync(csvPathDeliveries, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})

const jsonDeliveries = Papa.parse(csvDeliveries, {
    header: true,
})

const potm_each_season_func = () => {
    // initially getting potm count of players for each season
    const potm_data_each_season = jsonMatches.data.reduce((acc, matches) => {
        if (!acc[matches.season]) {
            acc[matches.season] = {}
        }
        if (!acc[matches.season][matches.player_of_match]) {
            acc[matches.season][matches.player_of_match] = 1
        } else {
            acc[matches.season][matches.player_of_match]++
        }

        return acc
    }, {})

    // console.log(potm_data_each_season)

    // now sorting and slicing to get the highest potm player each season
    const potm_each_season = Object.fromEntries(
        Object.entries(potm_data_each_season).map(([year, potm]) => [
            year,
            Object.fromEntries(
                Object.entries(potm)
                    .sort((player1, player2) => player2[1] - player1[1])
                    .slice(0, 1)
            ),
        ])
    )

    return potm_each_season
}
// console.log(potm_each_season)

// const potm_each_season = potm_each_season_func(jsonMatches)

// fs.writeFileSync(jsonPath, JSON.stringify(potm_each_season, null, 2))

module.exports = potm_each_season_func