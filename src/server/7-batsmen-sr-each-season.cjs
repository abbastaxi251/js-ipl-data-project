const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathMatches = path.join(__dirname, "../data/matches.csv")
const csvPathDeliveries = path.join(__dirname, "../data/deliveries.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/7-batsmen-sr-each-season.json"
// )

const csvMatches = fs.readFileSync(csvPathMatches, "utf-8")
const csvDeliveries = fs.readFileSync(csvPathDeliveries, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})

const jsonDeliveries = Papa.parse(csvDeliveries, {
    header: true,
})

const batsmen_sr_each_season_func = () => {
    const match_ids = jsonMatches.data.map((match) => {
        return { id: match.id, season: match.season }
    })
    // console.log(match_ids)
    // console.log(jsonDeliveries.data)

    //adding a column in the deliveries dataset which gives the season/year data
    const mergedDataset = jsonDeliveries.data.map((delivery) => {
        const yearObject = match_ids.find(
            (match) => match.id == String(delivery.match_id)
        )
        return { ...delivery, Year: yearObject.season }
    })
    // console.log(mergedDataset)

    const batsmen_data = mergedDataset.reduce((acc, deliveries) => {
        const batsman = deliveries.batsman
        const runs = Number(deliveries.batsman_runs)
        const season = deliveries.Year

        if (!acc[batsman]) {
            acc[batsman] = {}
        }

        if (!acc[batsman][season]) {
            acc[batsman][season] = {
                runs_scored: runs,
                balls_faced: 1,
                strike_rate: undefined,
            }
        } else {
            if (!acc[batsman][season].strike_rate) {
                acc[batsman][season].strike_rate =
                    (acc[batsman][season].runs_scored /
                        acc[batsman][season].balls_faced) *
                    100
            }
            acc[batsman][season].runs_scored += runs
            acc[batsman][season].balls_faced++
            acc[batsman][season].strike_rate =
                (acc[batsman][season].runs_scored /
                    acc[batsman][season].balls_faced) *
                100
        }

        return acc
    }, {})

    return batsmen_data
}

// const batsmen_sr_each_season = batsmen_sr_each_season_func(jsonMatches,jsonDeliveries)

// fs.writeFileSync(jsonPath, JSON.stringify(batsmen_sr_each_season, null, 2))

module.exports = batsmen_sr_each_season_func