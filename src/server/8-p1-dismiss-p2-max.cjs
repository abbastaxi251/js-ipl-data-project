const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathDeliveries = path.join(__dirname, "../data/deliveries.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/8-p1-dismiss-p2-max.json"
// )

const csvDeliveries = fs.readFileSync(csvPathDeliveries, "utf-8")

const jsonDeliveries = Papa.parse(csvDeliveries, {
    header: true,
})

const p1_dismiss_p2_max_func = () => {
    const p1_dismiss_p2_max = jsonDeliveries.data.reduce((acc, delivery) => {
        const player1 = delivery.bowler
        const player2 = delivery.player_dismissed
        const key = String(player1 + " dismissed " + player2 + " : ")

        if (
            player2 != undefined &&
            player2 != null &&
            player2.trim().length > 0
        ) {
            if (!acc[key]) {
                acc[key] = 1
            } else {
                acc[key]++
            }
        }
        return acc
    })
    let key_str = ""

    const max_dismiss_count = Object.keys(p1_dismiss_p2_max).reduce(
        (acc, key) => {
            if (Number(p1_dismiss_p2_max[key]) > acc) {
                acc = p1_dismiss_p2_max[key]
                key_str = `${key}`
            }

            return acc
        },
        0
    )

    let max_dismiss = {}
    max_dismiss[key_str] = max_dismiss_count

    return max_dismiss
}

// const p1_dismiss_p2_max = p1_dismiss_p2_max_func(jsonDeliveries)

// fs.writeFileSync(jsonPath, JSON.stringify(p1_dismiss_p2_max, null, 2))

module.exports = p1_dismiss_p2_max_func