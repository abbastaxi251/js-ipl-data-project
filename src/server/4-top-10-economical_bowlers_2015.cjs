const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

const csvPathMatches = path.join(__dirname, "../data/matches.csv")
const csvPathDeliveries = path.join(__dirname, "../data/deliveries.csv")
// const jsonPath = path.join(
//     __dirname,
//     "../public/output/4-top-10-economical-bowlers-2015.json"
// )

const csvMatches = fs.readFileSync(csvPathMatches, "utf-8")
const csvDeliveries = fs.readFileSync(csvPathDeliveries, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})

const jsonDeliveries = Papa.parse(csvDeliveries, {
    header: true,
})
const top_10_economical_bowlers_2015_func = () => {
    const matches_id_2015 = jsonMatches.data
        .filter((match) => {
            return match.season == "2015"
        })
        .map((match) => {
            return match.id
        })
    // console.log(matches_id_2015)
    // console.log(jsonDeliveries.data)

    const bowler_data = jsonDeliveries.data
        .filter((deliveries) => {
            return matches_id_2015.includes(String(deliveries.match_id))
        })
        .map((delivery) => {
            return [delivery.bowler, delivery.total_runs]
        })
        .reduce((acc, deliveries) => {
            const bowler = deliveries[0]
            const run = Number(deliveries[1])

            if (!acc[bowler]) {
                acc[bowler] = {
                    runs: run,
                    balls: 1,
                    economy: undefined,
                }
            } else {
                // for bowler delivery number 2 calculate its economy for its 1st delivery as earlier it was undefined
                if (!acc[bowler].economy) {
                    acc[bowler].economy =
                        (acc[bowler].run / acc[bowler].balls) * 6
                }
                acc[bowler].runs += run
                acc[bowler].balls++
                acc[bowler].economy = (acc[bowler].runs / acc[bowler].balls) * 6
            }

            return acc
        }, {})
    // console.log(bowler_data)
    // console.log(Object.entries(bowler_data))

    const bowler_dataArray = Object.entries(bowler_data)
        .map(([player, stats]) => {
            return {
                bowler: player,
                ...stats,
            }
        })
        .sort((a, b) => a.economy - b.economy)
        .slice(0, 10)

    return bowler_dataArray
}
// console.log(bowler_dataArray)

// const top_10_economical_bowlers_2015 = top_10_economical_bowlers_2015_func(jsonMatches,jsonDeliveries)

// fs.writeFileSync(jsonPath, JSON.stringify(top_10_economical_bowlers_2015, null, 2))

module.exports = top_10_economical_bowlers_2015_func
