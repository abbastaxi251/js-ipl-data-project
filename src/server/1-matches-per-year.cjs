const fs = require("fs")
const Papa = require("papaparse")
const path = require("path")

// const jsonPath = path.join(
//     __dirname,
//     "../public/output/1-matches-per-year.json"
// )

const csvPath = path.join(__dirname, "../data/matches.csv")

const csvMatches = fs.readFileSync(csvPath, "utf-8")

const jsonMatches = Papa.parse(csvMatches, {
    header: true,
})

// making an array of all the season number of the matches by extracting it from the matches.csv

const matches_per_year_func = () => {
    const matchesPerYear = jsonMatches.data
        .map((match) => match.season)
        // console.log(seasons) 

        // Using reduce to create a map with season as key and its count as value
        // Providing accumulator with the initial value as an empty object so it gets populated with key value pairs
        .reduce((acc, season) => {
            acc[season] = (acc[season] || 0) + 1
            return acc
        }, {})

    return matchesPerYear
}

// const matches_per_year = matches_per_year_func()

// fs.writeFileSync(jsonPath, JSON.stringify(matches_per_year, null, 2))

module.exports = matches_per_year_func
