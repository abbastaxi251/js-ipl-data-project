const express = require("express")
const dotenv = require("dotenv")
dotenv.config()

const port = process.env.PORT || 5000

const app = express()

const matches_per_year_func = require("./src/server/1-matches-per-year.cjs")
const matches_won_per_team_per_year_func = require("./src/server/2-matches-won-per-team-per-year.cjs")
const extra_runs_per_team_2016_func = require("./src/server/3-extra-runs-per-team-2016.cjs")
const top_10_economical_bowlers_2015_func = require("./src/server/4-top-10-economical_bowlers_2015.cjs")
const times_toss_match_win_func = require("./src/server/5-times-toss-match-win.cjs")
const potm_each_season_func = require("./src/server/6-potm-each-season.cjs")
const batsmen_sr_each_season_func = require("./src/server/7-batsmen-sr-each-season.cjs")
const p1_dismiss_p2_max_func = require("./src/server/8-p1-dismiss-p2-max.cjs")
const best_super_over_economy_func = require("./src/server/9-best-super-over-economy.cjs")

app.get("/matches-per-year", (req, res, next) => {
    const matches_per_year = matches_per_year_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(matches_per_year, null, 2))
    res.end()
})

app.get("/matches-won-per-team-per-year", (req, res, next) => {
    const matches_won_per_team_per_year = matches_won_per_team_per_year_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(matches_won_per_team_per_year, null, 2))
    res.end()
})

app.get("/extra-runs-per-team-2016", (req, res, next) => {
    const extra_runs_per_team_2016 = extra_runs_per_team_2016_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(extra_runs_per_team_2016, null, 2))
    res.end()
})

app.get("/top-10-economical-bowlers-2015", (req, res, next) => {
    const top_10_economical_bowlers_2015 = top_10_economical_bowlers_2015_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(top_10_economical_bowlers_2015, null, 2))
    res.end()
})

app.get("/times-toss-match-win", (req, res, next) => {
    const times_toss_match_win = times_toss_match_win_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(times_toss_match_win, null, 2))
    res.end()
})

app.get("/potm-each-season", (req, res, next) => {
    const potm_each_season = potm_each_season_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(potm_each_season, null, 2))
    res.end()
})

app.get("/batsmen-sr-each-season", (req, res, next) => {
    const batsmen_sr_each_season = batsmen_sr_each_season_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(batsmen_sr_each_season, null, 2))
    res.end()
})

app.get("/p1-dismiss-p2-max", (req, res, next) => {
    const p1_dismiss_p2_max = p1_dismiss_p2_max_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(p1_dismiss_p2_max, null, 2))
    res.end()
})

app.get("/best-super-over-economy", (req, res, next) => {
    const best_super_over_economy = best_super_over_economy_func()
    res.setHeader("200", { "Content - Type": "application/json" })
    res.write(JSON.stringify(best_super_over_economy, null, 2))
    res.end()
})

app.listen(port, () => {
    console.log(`listening on port ${port}`)
})
